

/// from ffmpeg (maybe 9+ are different in BF)
pub const EA_ADPCM_TABLE: [i32;20] = [
    0,  240,  460,  392,
    0,    0, -208, -220,
    0,    1,    3,    4,
    7,    8,   10,   11,
    0,   -1,   -3,   -4
];

/// copied from ffmpeg, used also for 4 bit value, so I think there is no direct replacement in rust
pub fn sign_extend<T>(val : T, bits : u8) -> i32
{
    unsafe { // safe on x86, and x64 architectures
        let shift = 8 * std::mem::size_of::<i32>() - bits as usize;
        let val = *(&val as *const T as *const u32);
        let u = val << shift;
        let s = *(&u as *const _ as *const i32);
        return s >> shift;
    }
}

/// copied from ffmpeg, and have no clue what kind of magic is used for the clipping
pub fn av_clip_int16_c(a :i32) -> i16
{
    let c = if (a+0x8000) & !0xFFFF != 0 {(a>>31) ^ 0x7FFF} else {a};
    let c = unsafe{*(&c as *const _ as *const i16)};
    c
}
